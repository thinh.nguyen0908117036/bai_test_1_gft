from pydoc import describe
from flask_restx import Namespace, fields


class UserDto:
    api = Namespace('user', description='user related operations')
    user = api.model('user', {
        'email': fields.String(required=True, description='user email address'),
        'username': fields.String(required=True, description='user username'),
        'password': fields.String(required=True, description='user password'),
        'user_id': fields.String(description='user Identifier')
    })


class AuthDto:
    api = Namespace('auth', description='authentication related operations')
    user_auth = api.model('auth_details', {
        'email': fields.String(required=True, description='The email address'),
        'password': fields.String(required=True, description='The user password '),
    })


class CartDto:
    api = Namespace('cart', description='cart api')
    cart = api.model('cart', {
        'productId': fields.String(required=True, description='518e8af7-6617-42a3-a227-b9be1b70fec8'),
        'quantity': fields.Integer(required=True, description=0)
    })

class CartItemDto:
    api = Namespace('cart_item', description='cart item api')
    cart_item = api.model('cart_item', {
        'productId': fields.String(required=True, description='518e8af7-6617-42a3-a227-b9be1b70fec8'),
        'quantity': fields.Integer(required=True, description=0)
    })