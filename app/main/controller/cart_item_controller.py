from logging.config import valid_ident
from pdb import post_mortem
from flask import request
from flask_restx import Resource

# from app.main.service.auth_helper import Auth
# from ..service.cart_service import save_new_cart
from ..util.dto import CartItemDto

api = CartItemDto.api
cart_item = CartItemDto.cart_item

@api.route('/cart-item')
class Cart(Resource):
    @api.doc('add to cart')
    @api.expect(cart_item, validate=True)
    def post(self):
        pass

    