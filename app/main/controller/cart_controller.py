from logging.config import valid_ident
from pdb import post_mortem
from flask import request
from flask_restx import Resource

from app.main.service.auth_helper import Auth
from ..service.cart_service import save_new_cart
from ..util.dto import CartDto

api = CartDto.api
cart = CartDto.cart

@api.route('/add')
class Cart(Resource):
    @api.doc('add to cart')
    @api.expect(cart, validate=True)
    def post(self):
        # get the post data
        post_data = request.json
        print("post data == ", post_data )
        return save_new_cart(data=post_data)

    
    # def get(self):
    #     pass


@api.route('/checkout')
class Checkout(Resource):
    @api.doc('check out')
    def get(self):
        print("Hello world")


# @api.route('/checkout')
# class CheckOut(Resource):
#     @api.doc('Check out cart')
#     @api.expect(cart, validate=True)
#     def post(self):
#         pass

#     def get(self):
#         pass