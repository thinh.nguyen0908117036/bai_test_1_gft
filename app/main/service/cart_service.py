import uuid
import datetime

from app.main import db
from app.main.model.cart import Cart
from typing import Dict

def save_new_cart(data: Dict[str, str]):
    # cart = Cart.query.filter_by(email=data['productId']).first()
    # if not cart:
    #     new_cart = Cart(
    #         productId=str(data["productId"],
    #         quantity=data['quantity'],
    #     )
        
    #     return save_changes(new_cart)
    # else:
    #     response_object = {
    #         'status': 'fail',
    #         'message': 'User already exists. Please Log in.',
    #     }
    #     return response_object, 409

    check_product = Cart.query.filter_by(product_id=data["productId"]).first()
    if check_product:
        # print("Số lượng hiện tại: ", check_product.quantity)
        # print("số lượng sản phẩm muốn thêm vào: ", data["quantity"])
        check_product.quantity = check_product.quantity + data["quantity"]
        save_changes(check_product)
    else:
        new_cart = Cart(
            product_id=data["productId"],
            quantity=data['quantity']
        )
        save_changes(new_cart)

    return "ok",201

def save_changes(data: Cart):
    db.session.add(data)
    db.session.commit()