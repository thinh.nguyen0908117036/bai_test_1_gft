
from .. import db, flask_bcrypt
import datetime
from app.main.model.blacklist import BlacklistToken
from ..config import key
import jwt
from typing import Union


class Add_To_Cart_Request(db.Model):
    __tablename__ = "add_to_cart_request"

    productId = db.Column(db.String(255), primary_key=True)
    quantity = db.Column(db.Integer, nullable=True)


    def __repr__(self):
        return "<User '{}'>".format(self.username)
