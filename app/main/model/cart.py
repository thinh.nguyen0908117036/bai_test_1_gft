
from ast import For
from .. import db, flask_bcrypt
import datetime
from app.main.model.blacklist import BlacklistToken
from ..config import key
import jwt
from typing import Union


class Cart(db.Model):
    __tablename__ = "cart"

    # cart_id = db.Column(db.String(255), primary_key=True)
    # userId = db.Column(db.String(255)) # ForeignKey
    # # quantity = db.Column(db.Integer, nullable=True)
    # subtotal_ex_tax = db.Column(db.Integer, nullable=True)
    # tax_total = db.Column(db.Integer, nullable=True)
    # total = db.Column(db.Integer, nullable=True)
    # cart_items = db.relationship('Cart_Item', backref='cart',
    #                             lazy='dynamic')
    # subtotal_ex_tax = db.Column(db.Integer, nullable=True)
    # tax_total = db.Column(db.Integer, nullable=True)
    # total = db.Column(db.Integer, nullable=True)
    
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    product_id = db.Column(db.String(200), unique=True, nullable=False)
    quantity = db.Column(db.Integer, nullable=False)
    # cart_id = db.Column(db.String, unique=True, default="lambda")
    cart_items = db.relationship('Cart_Item', backref='cart',
                                lazy='dynamic')


    def __repr__(self):
        return "<Product '{}'>".format(self.quantity)
