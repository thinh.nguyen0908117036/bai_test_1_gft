
from ast import For
from .. import db, flask_bcrypt
import datetime
from app.main.model.blacklist import BlacklistToken
from ..config import key
import jwt
from typing import Union


class Order(db.Model):
    __tablename__ = "order"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True, unique=True)


    order_id = db.Column(db.String(200), primary_key=True)

    # userId = db.Column(db.String(255)) # ForeignKey


    # order_items = db.relationship('Order_Item', backref='order',
    #                             lazy='dynamic')
    

    subtotal_ex_tax = db.Column(db.Integer, default=10)
    tax_total = db.Column(db.Integer, nullable=True)
    total = db.Column(db.Integer, nullable=True)
    payment_status = db.Column(db.String(255), nullable=True)

    order_items = db.relationship('Order_Item', backref='order',
                                lazy='dynamic')
    


    def __repr__(self):
        return "<Product '{}'>".format(self.name)
