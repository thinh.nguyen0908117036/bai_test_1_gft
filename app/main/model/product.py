
from .. import db, flask_bcrypt
import datetime
from app.main.model.blacklist import BlacklistToken
from ..config import key
import jwt
from typing import Union


class Product(db.Model):
    __tablename__ = "product"

    product_id = db.Column(db.String(255), primary_key=True)
    name = db.Column(db.String(255), unique=True, nullable=False)
    price = db.Column(db.Integer, nullable=True)


    def __repr__(self):
        return "<Product '{}'>".format(self.name)
