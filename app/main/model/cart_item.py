
from .. import db, flask_bcrypt
import datetime
from app.main.model.blacklist import BlacklistToken
from ..config import key
import jwt
from typing import Union

import uuid
class Cart_Item(db.Model):
    __tablename__ = "cart_item"

    # cart_item_id = db.Column(db.String(255), primary_key=True)
    # product_id = db.Column(db.String(255), primary_key=True)
    # quantity = db.Column(db.Integer, nullable=True)
    # subtotal_ex_tax = db.Column(db.Integer, nullable=True)
    # tax_total = db.Column(db.Integer, nullable=True)
    # total = db.Column(db.Integer, nullable=True)

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    product_id = db.Column(db.String(200), unique=True, nullable=False)
    quantity = db.Column(db.Integer, nullable=False)
    cart_item_id = db.Column(db.String(100), unique=True, default=lambda:uuid.uuid4())
    subtotal_ex_tax = db.Column(db.Float, unique=True, nullable=False)
    tax_total = db.Column(db.Float, unique=True, nullable=False)
    total =  db.Column(db.Float, nullable=False)
    cart_id = db.Column(db.Integer, db.ForeignKey('cart.id'))

    def __repr__(self):
        return "<Product '{}'>".format(self.name)
