
from ast import For
from .. import db, flask_bcrypt
import datetime
from app.main.model.blacklist import BlacklistToken
from ..config import key
import jwt
from typing import Union


class Order_Item(db.Model):
    __tablename__ = "order_item"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)

    order_item_id = db.Column(db.String(255), primary_key=True, default=lambda:uuid.uuid4())
    product_id = db.Column(db.String(255)) # ForeignKey
    quantity = db.Column(db.Integer, nullable=True)
    subtotal_ex_tax = db.Column(db.Integer, nullable=True)
    tax_total = db.Column(db.Integer, nullable=True)
    total = db.Column(db.Integer, nullable=True)
    order_id = db.Column(db.Integer, db.ForeignKey('order.id'))

    


    def __repr__(self):
        return "<Product '{}'>".format(self.name)
